module gitlab.com/rtfmkiesel/geopipe

go 1.20

require (
	github.com/asaskevich/govalidator v0.0.0-20230301143203-a9d515a09cc2
	github.com/oschwald/maxminddb-golang v1.10.0
	github.com/projectdiscovery/retryabledns v1.0.21
)

require (
	github.com/Mzack9999/go-http-digest-auth-client v0.6.1-0.20220414142836-eb8883508809 // indirect
	github.com/aymerick/douceur v0.2.0 // indirect
	github.com/gorilla/css v1.0.0 // indirect
	github.com/microcosm-cc/bluemonday v1.0.21 // indirect
	github.com/miekg/dns v1.1.50 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/projectdiscovery/retryablehttp-go v1.0.10 // indirect
	github.com/projectdiscovery/utils v0.0.4-0.20230117135930-7371ae6a739d // indirect
	github.com/rogpeppe/go-internal v1.9.0 // indirect
	github.com/saintfish/chardet v0.0.0-20120816061221-3af4cd4741ca // indirect
	go.uber.org/atomic v1.10.0 // indirect
	go.uber.org/multierr v1.8.0 // indirect
	golang.org/x/mod v0.6.0 // indirect
	golang.org/x/net v0.5.0 // indirect
	golang.org/x/sys v0.4.0 // indirect
	golang.org/x/text v0.6.0 // indirect
	golang.org/x/tools v0.2.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
